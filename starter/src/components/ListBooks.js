import { bookShelves } from "../constants/book-shelves";
import BookShelf from "./BookShelf";

const ListBooks = (props) => {
    const { books, setBooks, showSearchPage, setShowSearchpage } = props;

    return (
        <div className="list-books">
            <div className="list-books-title">
                <h1>MyReads</h1>
            </div>
            <div className="list-books-content">
                <div>
                    {bookShelves.map(shelf => (
                        <BookShelf
                            key={shelf.id}
                            booksOfShelf={books.filter(book => {
                                return book.shelf === shelf.id;
                            })}
                            showSearchPage={showSearchPage}
                            setBooks={setBooks}
                            shelfName={shelf.name}
                        />
                    ))}
                </div>
            </div>
            <div className="open-search">
                <button onClick={() => setShowSearchpage(!showSearchPage)}>Add a book</button>
            </div>
        </div>
    )
};

export default ListBooks;