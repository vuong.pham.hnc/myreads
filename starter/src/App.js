import "./App.css";
import { useEffect, useState } from "react";
import SearchBooks from "./components/SearchBooks";
import ListBooks from "./components/ListBooks";
import * as BooksAPI from "./BooksAPI";

const App = () => {
  const [books, setBooks] = useState([]);
  const [showSearchPage, setShowSearchpage] = useState(false);

  useEffect(() => {
    BooksAPI.getAll().then(books => {
      setBooks(books);
    });
  }, []);

  return (
    <div className="app">
      {showSearchPage ? (
        <SearchBooks
          books={books}
          setBooks={setBooks}
          showSearchPage={showSearchPage}
          setShowSearchpage={setShowSearchpage}
        />
      ) : (
        <ListBooks
          books={books}
          setBooks={setBooks}
          showSearchPage={showSearchPage}
          setShowSearchpage={setShowSearchpage}
        />
      )}
    </div>
  );
};

export default App;
